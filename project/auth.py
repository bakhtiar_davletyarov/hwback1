from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from . import db
from flask_login import login_user, logout_user, login_required
from .models import User

auth = Blueprint('auth', __name__)

@auth.route('/login', methods=['POST'])
def login_post():
    userlogin = request.form.get('userlogin')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(userlogin=userlogin).first()

    # check if the user actually exists
    # take the user-supplied password, hash it, and compare it to the hashed password in the database
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login')) # if the user doesn't exist or password is wrong, reload the page

    login_user(user, remember=remember)
    return redirect(url_for('main.profile'))

@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/registration')
def registration():
    return render_template('registration.html')

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))

@auth.route('/registration', methods=['POST'])
def registration_post():
    userlogin = request.form.get('userlogin')
    name = request.form.get('name')
    password = request.form.get('password')

    user = User.query.filter_by(userlogin=userlogin).first() 

    if user: # if a user is found, we want to redirect back to registration page so user can try again
        flash('The user already exists')
        return redirect(url_for('auth.registration'))

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(userlogin=userlogin, name=name, password=generate_password_hash(password, method='sha256'))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.login'))